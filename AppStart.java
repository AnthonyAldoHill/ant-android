package uk.co.anthonyaldohill.solesearch;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.sql.SQLException;

import static uk.co.anthonyaldohill.solesearch.CustomPanel.SwipeDirection.DISLIKE;
import static uk.co.anthonyaldohill.solesearch.CustomPanel.SwipeDirection.LIKE;
import static uk.co.anthonyaldohill.solesearch.CustomPanel.SwipeDirection.NONE;
import static uk.co.anthonyaldohill.solesearch.CustomPanel.destroyView;
import static uk.co.anthonyaldohill.solesearch.CustomPanel.swipeDirection;

public class AppStart extends AppCompatActivity {

    private boolean IS_APP_ACTIVE = false;
    private Boolean exit = false;
    RelativeLayout relLayout;
    CustomPanel customPanel, customPanel1, customPanel2;
    Intent storeIntent;
    Bundle bundle = new Bundle();
    int likeCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            getSupportActionBar().setCustomView(R.layout.action_bar);
        }
        storeIntent = new Intent(AppStart.this, StoreActivity.class);
        initActionBar();
        likeCount = 0;
        Log.i("count","likecount created = " + likeCount);
        StartCustomPanel();

        final Button filter_button = (Button) findViewById(R.id.filter_btn);
        filter_button.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_DOWN){
                    v.setBackgroundResource(R.drawable.filter_button_down);
                    return true;
                }
                else if(event.getAction() == MotionEvent.ACTION_UP) {
                    v.setBackgroundResource(R.drawable.filter_button);
                    Intent intent = new Intent(AppStart.this, FilterScreen.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.action_menu_top_in, R.anim.action_menu_top_out);
                    return true;
                }
                return false;
            }
        });

        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        final int width = displaymetrics.widthPixels;

        final ImageView likeButton = (ImageView) findViewById(R.id.like_button);
        likeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (relLayout.getChildAt(2).getScaleX() == 1 && relLayout.getChildAt(2).getScaleY() == 1) {
                    Runnable endAction = new Runnable() {
                        @Override
                        public void run() {
                            swipeDirection = LIKE;
                            destroyView = true;
                        }
                    };
                    relLayout.getChildAt(2).animate().translationX(width).scaleY(0.7f).setInterpolator(new DecelerateInterpolator(2.5f)).setDuration(500).withEndAction(endAction).start();
                }
            }
        });

        final ImageView dislikeButton = (ImageView) findViewById(R.id.dislike_button);
        dislikeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (relLayout.getChildAt(2).getScaleX() == 1 && relLayout.getChildAt(2).getScaleY() == 1) {
                    Runnable endAction = new Runnable() {
                        @Override
                        public void run() {
                            swipeDirection = DISLIKE;
                            destroyView = true;
                        }
                    };
                    relLayout.getChildAt(2).animate().translationX(-width).scaleY(0.7f).setInterpolator(new DecelerateInterpolator(2.5f)).setDuration(500).withEndAction(endAction).start();
                }
            }
        });

        ImageView infoButton = (ImageView) findViewById(R.id.info_button);
        infoButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AppStart.this, SingleViewActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.action_menu_bottom_in,R.anim.action_menu_bottom_out);
            }
        });

        DataBaseHelper myDbHelper;
        myDbHelper = new DataBaseHelper(this);

        try {

            myDbHelper.createDataBase();

        }
        catch (IOException ioe) {

            throw new Error("Unable to create database");

        }

        try {

            myDbHelper.openDataBase();

        }
        catch(SQLException sqle){

            throw new Error("SQLException");

        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        IS_APP_ACTIVE = true;
        final Handler handler = new Handler();
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (!IS_APP_ACTIVE) {
                    return;
                }
                updateCustomPanel();
                handler.postDelayed(this, 100);
            }
        });
        swipeDirection = NONE;
    }

    @Override
    protected void onResume() {
        super.onResume();
        IS_APP_ACTIVE = true;
        swipeDirection = NONE;
        likeCount = 0;
    }

    @Override
    protected void onStop() {
        super.onStop();
        IS_APP_ACTIVE = false;
    }

    @Override
    protected void onPause() {
        super.onPause();
        IS_APP_ACTIVE = false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        IS_APP_ACTIVE = false;
    }

    @Override
    public void onBackPressed() {
        if (exit) {
            finish(); // finish activity
        }
        else {
            Toast.makeText(this, "Press Back again to Exit.", Toast.LENGTH_SHORT).show();
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 3 * 1000);
        }
    }

    private void StartCustomPanel() {

        relLayout = (RelativeLayout) findViewById(R.id.add_custom_view);

        customPanel = new CustomPanel(this.getApplicationContext());
        customPanel.setScaleY(0.9f);
        customPanel.setScaleX(0.9f);

        customPanel1 = new CustomPanel(this.getApplicationContext());
        customPanel1.setScaleY(0.9f);
        customPanel1.setScaleX(0.9f);

        customPanel2 = new CustomPanel(this.getApplicationContext());

        relLayout.addView(customPanel);
        relLayout.addView(customPanel1);
        relLayout.addView(customPanel2);
    }

    protected void updateCustomPanel() {
        if (destroyView) {
            Log.i("Swipe Direction", "swipe direction = " + swipeDirection);
            ImageView notificationPopUp = (ImageView) findViewById(R.id.notification);
            if(swipeDirection == LIKE){
                likeCount++;
                notificationPopUp.setVisibility(View.VISIBLE);
                if(likeCount>0)
                    notificationPopUp.setBackgroundResource(R.drawable.notification_icon_overflow);
                Log.i("count", "" + likeCount);

                CustomPanel likePanel = (CustomPanel) relLayout.getChildAt(0);
                ImageView shoeImage = (ImageView) likePanel.findViewById(R.id.default_shoe);
                int imageResource = shoeImage.getResources().getIdentifier("default_shoe", "drawable", getPackageName());
                TextView shoeTitle = (TextView) findViewById(R.id.shoe_name);
                TextView shoePrice = (TextView) findViewById(R.id.shoe_price);
                TextView shoeColor = (TextView) findViewById(R.id.shoe_color);
                TextView shoeSize = (TextView) findViewById(R.id.shoe_size);
                sendToStore(imageResource, shoeTitle, shoePrice, shoeColor, shoeSize);
                //Update Algorithm function.
            }
            removeCustomPanel();
            CustomPanel nextChild1 = (CustomPanel) relLayout.getChildAt(1);
            nextChild1.animate().scaleX(1).scaleY(1).setDuration(500).start();

            /* Create new panel at 0.9 * original scale*/
            CustomPanel customPanelNew = new CustomPanel(this.getApplicationContext());
            customPanelNew.setScaleX(0.9f);
            customPanelNew.setScaleY(0.9f);
            relLayout.addView(customPanelNew, 0);
            destroyView = false;
        }
    }

    public void removeCustomPanel() {
        CustomPanel v = (CustomPanel) relLayout.getChildAt(2);
        relLayout.removeView(v);
    }

    private void initActionBar() {
        final ImageView actionBarBack = (ImageView) findViewById(R.id.action_bar_back);
        actionBarBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        final ImageView actionBarStore = (ImageView) findViewById(R.id.action_bar_store);
        actionBarStore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(storeIntent);
                overridePendingTransition(R.anim.action_menu_left_in, R.anim.action_menu_left_out);
            }
        });
    }

    private void sendToStore(int imageResource, TextView shoeTitle, TextView shoePrice, TextView shoeColor, TextView shoeSize){
        String s_title = shoeTitle.getText().toString();
        String s_price = shoePrice.getText().toString();
        String s_color = shoeColor.getText().toString();
        String s_size = shoeSize.getText().toString();
        bundle.putInt("shoeImage" + likeCount, imageResource);
        bundle.putString("shoeTitle" + likeCount, s_title);
        bundle.putString("shoePrice" + likeCount, s_price);
        bundle.putString("shoeColor" + likeCount, s_color);
        bundle.putString("shoeSize" + likeCount, s_size);
        bundle.putInt("likeCount", likeCount);
        Log.i("string", "resource entry name string = " + imageResource);
        storeIntent.putExtras(bundle);
    }
}