package uk.co.anthonyaldohill.solesearch;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.WindowManager;
import android.view.animation.DecelerateInterpolator;
import android.widget.RelativeLayout;

import static uk.co.anthonyaldohill.solesearch.CustomPanel.SwipeDirection.DISLIKE;
import static uk.co.anthonyaldohill.solesearch.CustomPanel.SwipeDirection.LIKE;
import static uk.co.anthonyaldohill.solesearch.CustomPanel.SwipeDirection.NONE;

public class CustomPanel extends RelativeLayout {

    private GestureDetector gDetector;
    static boolean destroyView = false;
    static SwipeDirection swipeDirection = NONE;
    DisplayMetrics metrics;

    public CustomPanel(Context context) {
        super(context);

        LayoutInflater inflater = LayoutInflater.from(getContext());
        inflater.inflate(R.layout.custom_panel, this, true);

        gDetector = new GestureDetector(context, new GestureListener(this));

        metrics = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        windowManager.getDefaultDisplay().getMetrics(metrics);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int MIN_DIST = 300;
        int START_X = 90;
        final int width = metrics.widthPixels;

        if(this.getScaleX() != 1 || this.getScaleY() != 1){
            return false;
        }
        if(event.getAction() == MotionEvent.ACTION_UP){
            if(this.getX() < START_X - MIN_DIST){
                Runnable endAction = new Runnable() {
                    @Override
                    public void run() {
                        swipeDirection = DISLIKE;
                        destroyView = true;
                    }
                };
                this.animate().translationX(-width).scaleY(0.7f).setInterpolator(new DecelerateInterpolator(2.5f)).setDuration(400).withEndAction(endAction).start();
            }
            else if (this.getX() > START_X + MIN_DIST){
                Runnable endAction = new Runnable() {
                    @Override
                    public void run() {
                        swipeDirection = LIKE;
                        destroyView = true;
                    }
                };
                this.animate().translationX(width).scaleY(0.7f).setInterpolator(new DecelerateInterpolator(2.5f)).setDuration(400).withEndAction(endAction).start();
            }
            else{
                this.animate().translationX(0).setInterpolator(new DecelerateInterpolator()).setDuration(500).start();
                swipeDirection = NONE;
                destroyView = false;
            }
        }
        return gDetector.onTouchEvent(event);
    }

    public void onMove(float dx, float dy) {
        this.setX(dx);
    }

    public enum SwipeDirection{
        LIKE,
        DISLIKE,
        NONE;
    }
}
