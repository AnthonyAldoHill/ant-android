package uk.co.anthonyaldohill.solesearch;

import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


public class FilterScreen extends AppCompatActivity {
    static boolean IS_MALE;
    static boolean IS_FEMALE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.filter_screen);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            getSupportActionBar().setCustomView(R.layout.action_bar);
        }
        initActionBar();
        loadUI();

        final Button male_button = (Button) findViewById(R.id.male_button);
        final Button female_button  =(Button) findViewById(R.id.female_button);
        male_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IS_MALE = true;
                IS_FEMALE = false;
                male_button.setBackgroundResource(R.drawable.round_button_select);
                female_button.setBackgroundResource(R.drawable.round_button);
            }
        });
        female_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IS_MALE = false;
                IS_FEMALE = true;
                female_button.setBackgroundResource(R.drawable.round_button_select);
                male_button.setBackgroundResource(R.drawable.round_button);
            }
        });

        final TextView textView1 = (TextView) findViewById(R.id.filter_text_1);
        final TextView textView2 = (TextView) findViewById(R.id.filter_text_2);
        final TextView textView3 = (TextView) findViewById(R.id.filter_text_3);
        final TextView textView4 = (TextView) findViewById(R.id.filter_text_4);
        Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/HVD_Poster.ttf");
        textView1.setTypeface(tf);
        textView2.setTypeface(tf);
        textView3.setTypeface(tf);
        textView4.setTypeface(tf);

        textView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LinearLayout linearBrand = (LinearLayout) findViewById(R.id.expand_brand);
                ViewGroup.LayoutParams params = linearBrand.getLayoutParams();
                if(linearBrand.getHeight() == 0){
                    textView1.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_expand_less_black_48px, 0);
                    params.height = ActionBar.LayoutParams.WRAP_CONTENT;
                    linearBrand.setLayoutParams(params);
                }
                else{
                    textView1.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_expand_more_black_48px, 0);
                    params.height = 0;
                    linearBrand.setLayoutParams(params);
                }
            }
        });
        textView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LinearLayout linearStyle = (LinearLayout) findViewById(R.id.expand_style);
                ViewGroup.LayoutParams params = linearStyle.getLayoutParams();
                if(linearStyle.getHeight() == 0){
                    textView2.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_expand_less_black_48px, 0);
                    params.height = ActionBar.LayoutParams.WRAP_CONTENT;
                    linearStyle.setLayoutParams(params);
                }
                else{
                    textView2.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_expand_more_black_48px, 0);
                    params.height = 0;
                    linearStyle.setLayoutParams(params);
                }
            }
        });
        textView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LinearLayout linearColor = (LinearLayout) findViewById(R.id.expand_colour);
                ViewGroup.LayoutParams params = linearColor.getLayoutParams();
                if(linearColor.getHeight() == 0){
                    textView3.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_expand_less_black_48px, 0);
                    params.height = ActionBar.LayoutParams.WRAP_CONTENT;
                    linearColor.setLayoutParams(params);
                }
                else{
                    textView3.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_expand_more_black_48px, 0);
                    params.height = 0;
                    linearColor.setLayoutParams(params);
                }
            }
        });
        textView4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LinearLayout linearSize = (LinearLayout) findViewById(R.id.expand_size);
                ViewGroup.LayoutParams params = linearSize.getLayoutParams();
                if(linearSize.getHeight() == 0){
                    textView4.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_expand_less_black_48px, 0);
                    params.height = ActionBar.LayoutParams.WRAP_CONTENT;
                    linearSize.setLayoutParams(params);
                }
                else{
                    textView4.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_expand_more_black_48px, 0);
                    params.height = 0;
                    linearSize.setLayoutParams(params);
                }
            }
        });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.action_menu_bottom_in, R.anim.action_menu_bottom_out);
    }

    @Override
    protected void onPause() {
        super.onPause();
        saveState();
    }

    @Override
    protected void onDestroy() {
        saveState();
        super.onDestroy();
    }


    private void initActionBar() {
        final ImageView actionBarBack = (ImageView) findViewById(R.id.action_bar_back);
        actionBarBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        final ImageView actionBarStore = (ImageView) findViewById(R.id.action_bar_store);
        actionBarStore.setVisibility(View.INVISIBLE);
    }


    private void loadUI() {
        SharedPreferences savedPref = getPreferences(MODE_PRIVATE);

        CheckBox checkAdidas = (CheckBox) findViewById(R.id.checkbox_adidas);
        CheckBox checkNike = (CheckBox) findViewById(R.id.checkbox_nike);
        CheckBox checkNewBalance = (CheckBox) findViewById(R.id.checkbox_newbalance);
        CheckBox checkReebok = (CheckBox) findViewById(R.id.checkbox_reebok);
        CheckBox checkTrainers = (CheckBox) findViewById(R.id.checkbox_trainers);
        CheckBox checkBoots = (CheckBox) findViewById(R.id.checkbox_boots);
        CheckBox checkSmartShoes = (CheckBox) findViewById(R.id.checkbox_smart);
        CheckBox checkBlack = (CheckBox) findViewById(R.id.checkbox_black);
        CheckBox checkWhite = (CheckBox) findViewById(R.id.checkbox_white);
        CheckBox checkBlue = (CheckBox) findViewById(R.id.checkbox_blue);
        CheckBox checkRed = (CheckBox) findViewById(R.id.checkbox_red);
        CheckBox checkGrey = (CheckBox) findViewById(R.id.checkbox_grey);
        CheckBox checkBrown = (CheckBox) findViewById(R.id.checkbox_brown);
        CheckBox checkGreen = (CheckBox) findViewById(R.id.checkbox_green);
        CheckBox checkOther = (CheckBox) findViewById(R.id.checkbox_other);
        CheckBox check3to5 = (CheckBox) findViewById(R.id.checkbox_3to5);
        CheckBox check5to7 = (CheckBox) findViewById(R.id.checkbox_5to7);
        CheckBox check7to10 = (CheckBox) findViewById(R.id.checkbox_7to10);
        CheckBox check10to13 = (CheckBox) findViewById(R.id.checkbox_10to13);
        Button male_button = (Button) findViewById(R.id.male_button);
        Button female_button = (Button) findViewById(R.id.female_button);

        boolean boolAdidas = savedPref.getBoolean("Checkbox", false);
        boolean boolNike = savedPref.getBoolean("Checkbox1", false);
        boolean boolNewBalance = savedPref.getBoolean("Checkbox2", false);
        boolean boolReebok = savedPref.getBoolean("Checkbox3", false);
        boolean boolTrainers = savedPref.getBoolean("Checkbox4", false);
        boolean boolBoots = savedPref.getBoolean("Checkbox5", false);
        boolean boolSmartShoes = savedPref.getBoolean("Checkbox6", false);
        boolean boolBlack = savedPref.getBoolean("Checkbox7", false);
        boolean boolWhite = savedPref.getBoolean("Checkbox8", false);
        boolean boolBlue = savedPref.getBoolean("Checkbox9", false);
        boolean boolRed = savedPref.getBoolean("Checkbox10", false);
        boolean boolGrey = savedPref.getBoolean("Checkbox11", false);
        boolean boolBrown = savedPref.getBoolean("Checkbox12", false);
        boolean boolGreen = savedPref.getBoolean("Checkbox13", false);
        boolean boolOther = savedPref.getBoolean("Checkbox14", false);
        boolean bool3to5 = savedPref.getBoolean("Checkbox15", false);
        boolean bool5to7 = savedPref.getBoolean("Checkbox16", false);
        boolean bool7to10 = savedPref.getBoolean("Checkbox17", false);
        boolean bool10to13 = savedPref.getBoolean("Checkbox18", false);
        boolean boolMale = savedPref.getBoolean("Male Button", false);
        boolean boolFemale = savedPref.getBoolean("Female Button", false);

        checkAdidas.setChecked(boolAdidas);
        checkNike.setChecked(boolNike);
        checkNewBalance.setChecked(boolNewBalance);
        checkReebok.setChecked(boolReebok);
        checkTrainers.setChecked(boolTrainers);
        checkBoots.setChecked(boolBoots);
        checkSmartShoes.setChecked(boolSmartShoes);
        checkBlack.setChecked(boolBlack);
        checkWhite.setChecked(boolWhite);
        checkBlue.setChecked(boolBlue);
        checkRed.setChecked(boolRed);
        checkGrey.setChecked(boolGrey);
        checkBrown.setChecked(boolBrown);
        checkGreen.setChecked(boolGreen);
        checkOther.setChecked(boolOther);
        check3to5.setChecked(bool3to5);
        check5to7.setChecked(bool5to7);
        check7to10.setChecked(bool7to10);
        check10to13.setChecked(bool10to13);
        male_button.setBackgroundResource(R.drawable.round_button_select);
        female_button.setBackgroundResource(R.drawable.round_button_select);

        if(boolMale){
            male_button.setBackgroundResource(R.drawable.round_button_select);
            female_button.setBackgroundResource(R.drawable.round_button);
        }
        if(boolFemale){
            female_button.setBackgroundResource(R.drawable.round_button_select);
            male_button.setBackgroundResource(R.drawable.round_button);
        }
    }


    private void saveState() {
        SharedPreferences savePref=getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor editor=savePref.edit();

        CheckBox checkAdidas = (CheckBox) findViewById(R.id.checkbox_adidas);
        CheckBox checkNike = (CheckBox) findViewById(R.id.checkbox_nike);
        CheckBox checkNewBalance = (CheckBox) findViewById(R.id.checkbox_newbalance);
        CheckBox checkReebok = (CheckBox) findViewById(R.id.checkbox_reebok);
        CheckBox checkTrainers = (CheckBox) findViewById(R.id.checkbox_trainers);
        CheckBox checkBoots = (CheckBox) findViewById(R.id.checkbox_boots);
        CheckBox checkSmartShoes = (CheckBox) findViewById(R.id.checkbox_smart);
        CheckBox checkBlack = (CheckBox) findViewById(R.id.checkbox_black);
        CheckBox checkWhite = (CheckBox) findViewById(R.id.checkbox_white);
        CheckBox checkBlue = (CheckBox) findViewById(R.id.checkbox_blue);
        CheckBox checkRed = (CheckBox) findViewById(R.id.checkbox_red);
        CheckBox checkGrey = (CheckBox) findViewById(R.id.checkbox_grey);
        CheckBox checkBrown = (CheckBox) findViewById(R.id.checkbox_brown);
        CheckBox checkGreen = (CheckBox) findViewById(R.id.checkbox_green);
        CheckBox checkOther = (CheckBox) findViewById(R.id.checkbox_other);
        CheckBox check3to5 = (CheckBox) findViewById(R.id.checkbox_3to5);
        CheckBox check5to7 = (CheckBox) findViewById(R.id.checkbox_5to7);
        CheckBox check7to10 = (CheckBox) findViewById(R.id.checkbox_7to10);
        CheckBox check10to13 = (CheckBox) findViewById(R.id.checkbox_10to13);

        boolean boolAdidas = checkAdidas.isChecked();
        boolean boolNike = checkNike.isChecked();
        boolean boolNewBalance = checkNewBalance.isChecked();
        boolean boolReebok = checkReebok.isChecked();
        boolean boolTrainers = checkTrainers.isChecked();
        boolean boolBoots = checkBoots.isChecked();
        boolean boolSmartShoes = checkSmartShoes.isChecked();
        boolean boolBlack = checkBlack.isChecked();
        boolean boolWhite = checkWhite.isChecked();
        boolean boolBlue = checkBlue.isChecked();
        boolean boolRed = checkRed.isChecked();
        boolean boolGrey = checkGrey.isChecked();
        boolean boolBrown = checkBrown.isChecked();
        boolean boolGreen = checkGreen.isChecked();
        boolean boolOther = checkOther.isChecked();
        boolean bool3to5 = check3to5.isChecked();
        boolean bool5to7 = check5to7.isChecked();
        boolean bool7to10 = check7to10.isChecked();
        boolean bool10to13 = check10to13.isChecked();
        boolean boolMale = IS_MALE;
        boolean boolFemale = IS_FEMALE;


        editor.putBoolean("Checkbox", boolAdidas);
        editor.putBoolean("Checkbox1", boolNike);
        editor.putBoolean("Checkbox2", boolNewBalance);
        editor.putBoolean("Checkbox3", boolReebok);
        editor.putBoolean("Checkbox4", boolTrainers);
        editor.putBoolean("Checkbox5", boolBoots);
        editor.putBoolean("Checkbox6", boolSmartShoes);
        editor.putBoolean("Checkbox7", boolBlack);
        editor.putBoolean("Checkbox8", boolWhite);
        editor.putBoolean("Checkbox9", boolBlue);
        editor.putBoolean("Checkbox10", boolRed);
        editor.putBoolean("Checkbox11", boolGrey);
        editor.putBoolean("Checkbox12", boolBrown);
        editor.putBoolean("Checkbox13", boolGreen);
        editor.putBoolean("Checkbox14", boolOther);
        editor.putBoolean("Checkbox15", bool3to5);
        editor.putBoolean("Checkbox16", bool5to7);
        editor.putBoolean("Checkbox17", bool7to10);
        editor.putBoolean("Checkbox18", bool10to13);
        editor.putBoolean("Male Button", boolMale);
        editor.putBoolean("Female Button", boolFemale);

        editor.apply();
    }

}
