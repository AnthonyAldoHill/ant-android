package uk.co.anthonyaldohill.solesearch;

import android.view.GestureDetector;
import android.view.MotionEvent;


class GestureListener extends GestureDetector.SimpleOnGestureListener {

    private CustomPanel customPanel;

    public GestureListener(CustomPanel customPanel){
        this.customPanel = customPanel;
    }

    @Override
    public boolean onDown(MotionEvent event) {
        return true;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        float distX = e2.getRawX() - e1.getX();
        float distY = 0;
//        customPanel.animate().scaleX(0.8f).scaleY(0.8f).start();
        customPanel.onMove(distX, distY);
        return true;
    }
}
