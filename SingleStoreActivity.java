package uk.co.anthonyaldohill.solesearch;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;


public class SingleStoreActivity extends AppCompatActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.single_store);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            getSupportActionBar().setCustomView(R.layout.action_bar);
        }
        initActionBar();

        final ViewPager viewPager = (ViewPager) findViewById(R.id.view_pager);
        ImageSlider slider = new ImageSlider(this);
        viewPager.setAdapter(slider);

        final ImageView pos1 = (ImageView) findViewById(R.id.photo1);
        final ImageView pos2 = (ImageView) findViewById(R.id.photo2);
        final ImageView pos3 = (ImageView) findViewById(R.id.photo3);
        final ImageView pos4 = (ImageView) findViewById(R.id.photo4);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if(position == 0){
                    pos1.setImageResource(R.drawable.current_photo);
                    pos2.setImageResource(R.drawable.next_photo);
                    pos3.setImageResource(R.drawable.next_photo);
                    pos4.setImageResource(R.drawable.next_photo);
            }
                else if(position == 1) {
                    pos1.setImageResource(R.drawable.next_photo);
                    pos2.setImageResource(R.drawable.current_photo);
                    pos3.setImageResource(R.drawable.next_photo);
                    pos4.setImageResource(R.drawable.next_photo);
                }
                else if(position == 2) {
                    pos1.setImageResource(R.drawable.next_photo);
                    pos2.setImageResource(R.drawable.next_photo);
                    pos3.setImageResource(R.drawable.current_photo);
                    pos4.setImageResource(R.drawable.next_photo);
                }
                else if(position == 3) {
                    pos1.setImageResource(R.drawable.next_photo);
                    pos2.setImageResource(R.drawable.next_photo);
                    pos3.setImageResource(R.drawable.next_photo);
                    pos4.setImageResource(R.drawable.current_photo);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        openWebPage();
    }

    private void initActionBar() {
        final ImageView actionBarBack = (ImageView) findViewById(R.id.action_bar_back);
        actionBarBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        final ImageView actionBarStore = (ImageView) findViewById(R.id.action_bar_store);
        actionBarStore.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.action_menu_right_in, R.anim.action_menu_right_out);
    }

    private void openWebPage(){
        RelativeLayout store1 = (RelativeLayout) findViewById(R.id.store1);
        store1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri webpage = Uri.parse("http://www.asos.com/");
                Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivity(intent);
                }
            }
        });
        RelativeLayout store2 = (RelativeLayout) findViewById(R.id.store2);
        store2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri webpage = Uri.parse("https://www.footlocker.co.uk/");
                Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivity(intent);
                }
            }
        });
        RelativeLayout store3 = (RelativeLayout) findViewById(R.id.store3);
        store3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri webpage = Uri.parse("http://www.footasylum.com/");
                Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivity(intent);
                }
            }
        });
        RelativeLayout store4 = (RelativeLayout) findViewById(R.id.store4);
        store4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri webpage = Uri.parse("https://www.jdsports.co.uk/");
                Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivity(intent);
                }
            }
        });
    }
}
