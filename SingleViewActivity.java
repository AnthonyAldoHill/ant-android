package uk.co.anthonyaldohill.solesearch;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;


public class SingleViewActivity extends AppCompatActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.single_view);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            getSupportActionBar().setCustomView(R.layout.action_bar);
        }
        initActionBar();

        final ViewPager viewPager = (ViewPager) findViewById(R.id.view_pager);
        ImageSlider slider = new ImageSlider(this);
        viewPager.setAdapter(slider);

        final ImageView pos1 = (ImageView) findViewById(R.id.photo1);
        final ImageView pos2 = (ImageView) findViewById(R.id.photo2);
        final ImageView pos3 = (ImageView) findViewById(R.id.photo3);
        final ImageView pos4 = (ImageView) findViewById(R.id.photo4);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                Log.i("pos", "position is: " + position);
                if(position == 0){
                    Log.i("pos","position 0 should be position " + position);
                    pos1.setImageResource(R.drawable.current_photo);
                    pos2.setImageResource(R.drawable.next_photo);
                    pos3.setImageResource(R.drawable.next_photo);
                    pos4.setImageResource(R.drawable.next_photo);
            }
                else if(position == 1) {
                    Log.i("pos","position 1 should be position " + position);
                    pos1.setImageResource(R.drawable.next_photo);
                    pos2.setImageResource(R.drawable.current_photo);
                    pos3.setImageResource(R.drawable.next_photo);
                    pos4.setImageResource(R.drawable.next_photo);
                }
                else if(position == 2) {
                    Log.i("pos","position 2 should be position " + position);
                    pos1.setImageResource(R.drawable.next_photo);
                    pos2.setImageResource(R.drawable.next_photo);
                    pos3.setImageResource(R.drawable.current_photo);
                    pos4.setImageResource(R.drawable.next_photo);
                }
                else if(position == 3) {
                    Log.i("pos","position 3 should be position " + position);
                    pos1.setImageResource(R.drawable.next_photo);
                    pos2.setImageResource(R.drawable.next_photo);
                    pos3.setImageResource(R.drawable.next_photo);
                    pos4.setImageResource(R.drawable.current_photo);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void initActionBar() {
        final ImageView actionBarBack = (ImageView) findViewById(R.id.action_bar_back);
        actionBarBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        final ImageView actionBarStore = (ImageView) findViewById(R.id.action_bar_store);
        actionBarStore.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.action_menu_top_in, R.anim.action_menu_top_out);
    }
}
