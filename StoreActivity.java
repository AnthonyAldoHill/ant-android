package uk.co.anthonyaldohill.solesearch;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import static uk.co.anthonyaldohill.solesearch.CustomPanel.swipeDirection;

public class StoreActivity extends AppCompatActivity {
    boolean isDeleting = false;
    GridView gridview;
    ImageAdapter imageAdapter = new ImageAdapter(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            getSupportActionBar().setCustomView(R.layout.action_bar);
        }
        initActionBar();

        gridview = (GridView) findViewById(R.id.gridview);
        gridview.setAdapter(imageAdapter);

        Bundle bundle = getIntent().getExtras();
        if(bundle != null) {
            if(swipeDirection == CustomPanel.SwipeDirection.LIKE) {
                int likeCount = bundle.getInt("likeCount");
                for(int j = 1; j <= likeCount; j++){
                    int imageResource = bundle.getInt("shoeImage" + j);
                    ImageAdapter.addImage(imageResource);
                }
            }
        }

        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id){
                if(isDeleting){
                    v.setBackgroundResource(R.color.colorPanel);
                }
                else {
                    // Send intent to SingleViewActivity
                    Intent i = new Intent(getApplicationContext(), SingleStoreActivity.class);
                    // Pass image index
                    i.putExtra("id", position);
                    startActivity(i);
                    overridePendingTransition(R.anim.action_menu_left_in, R.anim.action_menu_left_out);
                }
            }
        });

        gridview.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                isDeleting = true;
                view.setBackgroundColor(Color.rgb(80, 80, 80));
                return true;
            }
        });

        TextView textView = (TextView) findViewById(R.id.store_title);
        Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/HVD_Poster.ttf");
        textView.setTypeface(tf);
    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
        overridePendingTransition(R.anim.action_menu_right_in, R.anim.action_menu_right_out);
    }

    private void initActionBar() {
        final ImageView actionBarBack = (ImageView) findViewById(R.id.action_bar_back);
        actionBarBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        final ImageView actionBarStore = (ImageView) findViewById(R.id.action_bar_store);
        actionBarStore.setVisibility(View.INVISIBLE);
    }
}
